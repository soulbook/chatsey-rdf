Server = require("./node_modules/rdfstore/server.js").Server

# Server.start()
options = {}
for p of Server.defaultOptions
  options[p] = Server.defaultOptions[p].def  unless options[p]?

options["store-engine"] = "mongodb"
options["store-name"] = "app18760637"
options["store-mongo-domain"] = "chatsey:chatsey3333@paulo.mongohq.com"
options["store-mongo-port"] = 10096
options["port"] = process.env.PORT or 8080
options["protocol"] = "http"

https = require("https")
http = require("http")
fs = require("fs")
express = require("express")

app = express()
# app.set "views", __dirname + "/views"
# app.set "view engine", "jade"
engines = require("consolidate")
# app.set "views", __dirname + "/views"
# app.engine "jade", engines.jade
# app.set "view engine", "jade"

request = require("request")

app.use '/', express.static(__dirname + '/views')


app.get "/resource", (req, res) ->
  res.redirect "/resource.html"  
  
app.get "/ontology", (req, res) ->
  res.redirect "/ontology.html"  
  
queryNode = (node, req, res)->  
  request.post "http://rdf.chatsey.com/sparql",
    form:
      query: """
      construct { <#{node}> ?p ?o } where { <#{node}> ?p ?o }
      """
  , (err, response) ->
    request.post "http://rhizomik.net/redefer-services/rdf2html",
      form:
        rdf: response.body
        mode: "html"
        namespaces: "false"
        language: "en"
    ,(err, response) ->
      html = response.body
      html = html.replace "<title>Rhizomik - ReDeFer - RDF2HTML</title>", "<title>Prestie Ontology #{node}</title>"
      html = html.replace "/redefer-services/style/rhizomer.css", "http://rhizomik.net/redefer-services/style/rhizomer.css"
      html = html.replace "/redefer-services/images/rhizomer.small.png", "http://rhizomik.net/redefer-services/images/rhizomer.small.png"
      res.writeHead(200, {'Content-Type': 'text/html'});
      res.write(html);
      res.end();

queryNodeWithID = (type, id, req, res)->  
  request.post "http://rdf.chatsey.com/sparql",
    form:
      query: """
      PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>        
      construct { ?n a <#{type}> . ?n rdf:nodeID <#{id}> ?n ?p ?o } where { ?n a <#{type}> . ?n rdf:nodeID <#{id}> . ?n ?p ?o }
      """
  , (err, response) ->
    request.post "http://rhizomik.net/redefer-services/rdf2html",
      form:
        rdf: response.body
        mode: "html"
        namespaces: "false"
        language: "en"
    ,(err, response) ->
      html = response.body
      html = html.replace "<title>Rhizomik - ReDeFer - RDF2HTML</title>", "<title>Prestie Ontology #{node}</title>"
      html = html.replace "/redefer-services/style/rhizomer.css", "http://rhizomik.net/redefer-services/style/rhizomer.css"
      html = html.replace "/redefer-services/images/rhizomer.small.png", "http://rhizomik.net/redefer-services/images/rhizomer.small.png"
      res.writeHead(200, {'Content-Type': 'text/html'});
      res.write(html);
      res.end();
  
app.get "/ontology/class/:class", (req, res) ->
  queryNode "http://rdf.chatsey.com/ontology/class/#{req.params.class}", req,res

app.get "/ontology/property/:property", (req, res) ->
  queryNode "http://rdf.chatsey.com/ontology/property/#{req.params.property}", req,res

app.get "/resource/:class/:id", (req, res) ->
  queryNode "http://rdf.chatsey.com/resource/#{req.params.class}/#{req.params.id}", req,res
  
  # queryNodeWithID "http://rdf.chatsey.com/ontology/class/#{req.params.class}", "#{req.params.id}", req,res

Server.startStore options, ->
  app.use Server.routeRequest(options)
  app.listen options["port"]
